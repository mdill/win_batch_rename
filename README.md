# Windows Batch Rename

## Purpose

This small line of code is designed to rename parts of filename strings in a
working directory.

Just navigate to the directory with the target files, open PowerShell in that
directory, copy/paste this line of code, and replace the information in the
quotes.  The first set of quotes is the target string, and the second set of
quotes is what you would like to reploce the target with.

## Download

    cd ~/
    git clone https://bitbucket.org/mdill/win_batch_rename.git

## License

This project is licensed under the BSD License - see the [LICENSE.md](https://bitbucket.org/mdill/win_batch_rename/src/0aaa10ad31be0af37eb2861061bbca02053890ec/LICENSE.txt?at=master) file for
details.

